local lume
local HC

local objects
local screen

--- LÖVE callbacks ---
function love.load()
  love.math.setRandomSeed(42)

  screen = {
    width = 1024,
    height = 768
  }

  love.filesystem.setRequirePath(love.filesystem.getRequirePath() .. ";lib/?.lua;lib/?/init.lua;lib/lume/?.lua")

  lume = require "lume"
  HC = require "HC"

  local blockSize = 80
  objects = {
    ball = {
      shape = HC.circle(100, screen.height/2 + 30, 20),
      velocity = {x = 300, y = 0}
    },
    block = HC.rectangle(screen.width/2 - blockSize/2, screen.height/2 - blockSize/2 - 100,
                         blockSize, blockSize)
  }
  -- Adjust the angle of the ball's direction of travel.
  local magnitude = math.sqrt(objects.ball.velocity.x * objects.ball.velocity.x +
                                objects.ball.velocity.y * objects.ball.velocity.y)
  local angle = math.atan2(objects.ball.velocity.y, objects.ball.velocity.x)
  angle = angle - math.rad(10)
  local v_x, v_y = lume.vector(angle, magnitude)
  objects.ball.velocity = {x = v_x, y = v_y}

  -- Initial graphics setup.
  love.window.setMode(screen.width, screen.height)
end

function love.update(dt)
  for other, separatingVector in pairs(HC.collisions(objects.ball.shape)) do
    print(other, separatingVector)

    local separatingMagnitude = math.sqrt(
    separatingVector.x * separatingVector.x +
      separatingVector.y * separatingVector.y)
    
    -- Take the abs of the objects.ball velocity because we want to use the sign
    -- from the collision vector.
    objects.ball.velocity = {
      x = math.abs(objects.ball.velocity.x) * separatingVector.x / separatingMagnitude,
      y = math.abs(objects.ball.velocity.y) * separatingVector.y / separatingMagnitude
    }
  end

  objects.ball.shape:move(objects.ball.velocity.x * dt, objects.ball.velocity.y * dt)
end

function love.keypressed(key)
  if key == "escape" then
    love.event.quit("restart")
  end
end

function love.draw()
  love.graphics.setColor(0.95, 0.95, 0.95)
  objects.ball.shape:draw("fill")
  objects.block:draw("fill")
end
